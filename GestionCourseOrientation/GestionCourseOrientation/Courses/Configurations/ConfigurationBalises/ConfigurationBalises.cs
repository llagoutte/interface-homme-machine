﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionCourseOrientation.Courses.Configurations.ConfigurationsBalises;

namespace GestionCourseOrientation
{
    public partial class ConfigurationBalises : Form
    {
        private List<Balise> Balises;
        public ConfigurationBalises(ref List<Balise> O_Balises)
        {
            InitializeComponent();
            Balises = O_Balises;
            AfficherListe();
        }

        private void AfficherListe()
        {
            Grille.Rows.Clear();
            foreach (Balise AfficherBalises in Balises)
            {
                Grille.Rows.Add(AfficherBalises.Numero, AfficherBalises.Identifiant, AfficherBalises.GPS, "Supprimer", "Télécharger");
            }
        }

        private void BouttonAjouter_Click(object sender, EventArgs e)
        {
            AjouterBalise AjoutDeBalise = new AjouterBalise(ref Balises);
            AjoutDeBalise.ShowDialog();
            AfficherListe();
        }

        private void Grille_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3) 
            {
                 Balises.Remove(Balises[e.RowIndex]);   // Bouton Supprimer (Pour supprimer une balise)
                 AfficherListe();
            }

            if (e.ColumnIndex == 4)
            {
                    // Bouton télécharger (Télécharger dans le MPC)
            }
        }
    }
}
