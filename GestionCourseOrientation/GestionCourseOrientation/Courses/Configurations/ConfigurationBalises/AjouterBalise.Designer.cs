﻿namespace GestionCourseOrientation.Courses.Configurations.ConfigurationsBalises
{
    partial class AjouterBalise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelNumero = new System.Windows.Forms.Label();
            this.LabelGPS = new System.Windows.Forms.Label();
            this.BoutonAjouter = new System.Windows.Forms.Button();
            this.LabelIdentifiant = new System.Windows.Forms.Label();
            this.ComboBoxNum = new System.Windows.Forms.ComboBox();
            this.ComboBoxId = new System.Windows.Forms.ComboBox();
            this.TextBoxGPS = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LabelNumero
            // 
            this.LabelNumero.AutoSize = true;
            this.LabelNumero.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNumero.Location = new System.Drawing.Point(12, 31);
            this.LabelNumero.Name = "LabelNumero";
            this.LabelNumero.Size = new System.Drawing.Size(84, 18);
            this.LabelNumero.TabIndex = 0;
            this.LabelNumero.Text = "Numéro :";
            // 
            // LabelGPS
            // 
            this.LabelGPS.AutoSize = true;
            this.LabelGPS.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelGPS.Location = new System.Drawing.Point(12, 112);
            this.LabelGPS.Name = "LabelGPS";
            this.LabelGPS.Size = new System.Drawing.Size(53, 18);
            this.LabelGPS.TabIndex = 1;
            this.LabelGPS.Text = "GPS :";
            // 
            // BoutonAjouter
            // 
            this.BoutonAjouter.Location = new System.Drawing.Point(255, 150);
            this.BoutonAjouter.Name = "BoutonAjouter";
            this.BoutonAjouter.Size = new System.Drawing.Size(118, 38);
            this.BoutonAjouter.TabIndex = 2;
            this.BoutonAjouter.Text = "Ajouter";
            this.BoutonAjouter.UseVisualStyleBackColor = true;
            this.BoutonAjouter.Click += new System.EventHandler(this.BoutonAjouter_Click);
            // 
            // LabelIdentifiant
            // 
            this.LabelIdentifiant.AutoSize = true;
            this.LabelIdentifiant.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelIdentifiant.Location = new System.Drawing.Point(12, 70);
            this.LabelIdentifiant.Name = "LabelIdentifiant";
            this.LabelIdentifiant.Size = new System.Drawing.Size(108, 18);
            this.LabelIdentifiant.TabIndex = 6;
            this.LabelIdentifiant.Text = "Identifiant :";
            // 
            // ComboBoxNum
            // 
            this.ComboBoxNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxNum.FormattingEnabled = true;
            this.ComboBoxNum.Location = new System.Drawing.Point(207, 27);
            this.ComboBoxNum.Name = "ComboBoxNum";
            this.ComboBoxNum.Size = new System.Drawing.Size(166, 21);
            this.ComboBoxNum.TabIndex = 7;
            // 
            // ComboBoxId
            // 
            this.ComboBoxId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxId.FormattingEnabled = true;
            this.ComboBoxId.Location = new System.Drawing.Point(207, 67);
            this.ComboBoxId.Name = "ComboBoxId";
            this.ComboBoxId.Size = new System.Drawing.Size(166, 21);
            this.ComboBoxId.TabIndex = 8;
            // 
            // TextBoxGPS
            // 
            this.TextBoxGPS.Location = new System.Drawing.Point(207, 109);
            this.TextBoxGPS.Name = "TextBoxGPS";
            this.TextBoxGPS.Size = new System.Drawing.Size(166, 20);
            this.TextBoxGPS.TabIndex = 9;
            // 
            // AjouterBalise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 200);
            this.Controls.Add(this.TextBoxGPS);
            this.Controls.Add(this.ComboBoxId);
            this.Controls.Add(this.ComboBoxNum);
            this.Controls.Add(this.LabelIdentifiant);
            this.Controls.Add(this.BoutonAjouter);
            this.Controls.Add(this.LabelGPS);
            this.Controls.Add(this.LabelNumero);
            this.Name = "AjouterBalise";
            this.Text = "Ajouter une balise";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelNumero;
        private System.Windows.Forms.Label LabelGPS;
        private System.Windows.Forms.Button BoutonAjouter;
        private System.Windows.Forms.Label LabelIdentifiant;
        private System.Windows.Forms.ComboBox ComboBoxNum;
        private System.Windows.Forms.ComboBox ComboBoxId;
        private System.Windows.Forms.TextBox TextBoxGPS;
    }
}