﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation.Courses.Configurations.ConfigurationsBalises
{
    public partial class AjouterBalise : Form
    {
        private List<Balise> Balises;

        public AjouterBalise(ref List<Balise> O_Balises)
        {
            InitializeComponent();
            Balises = O_Balises;
            ComboBoxId.DataSource = new [] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"};
            ComboBoxNum.DataSource = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25" };
        }


        private void BoutonAjouter_Click(object sender, EventArgs e)
        {
            if(TextBoxGPS.Text.Length > 0)
            {
                try
                {
                    bool ExisteDeja = false, Enregistrer = false;
                    Balise NouvelleBalise = new Balise();
                    NouvelleBalise.Numero = (byte)Convert.ToInt16(ComboBoxNum.Text);
                    NouvelleBalise.Identifiant = (byte)Convert.ToInt16(ComboBoxId.Text); ;
                    NouvelleBalise.GPS = Convert.ToSingle(TextBoxGPS.Text);

                    foreach (Balise Balise in Balises)
                    {
                        if(Balise.Numero == NouvelleBalise.Numero)
                        {
                            ExisteDeja = true;
                        }
                        if(Balise.Identifiant == NouvelleBalise.Identifiant)
                        {
                            ExisteDeja = true;
                        }
                    }

                    if (ExisteDeja == true)
                    {
                        MessageBox.Show("Il existe déjà une balise avec ce même Numéro ou Identifiant");
                    }
                    else
                    {
                        Enregistrer = NouvelleBalise.Sauvegarder();
                        if (Enregistrer == false)
                        {
                            MessageBox.Show("Erreur lors de l'enregistrement en BD !");
                        }
                        else
                        {
                            Balises.Add(NouvelleBalise);
                            this.Close();
                        }
                    }
                 }
                catch
                {
                    MessageBox.Show("La Position GPS doit être renseigné correctement !");
                }
            }
            else
            {
                MessageBox.Show("Veuillez renseigner tous les champs !");
            }
        }
    }
}
