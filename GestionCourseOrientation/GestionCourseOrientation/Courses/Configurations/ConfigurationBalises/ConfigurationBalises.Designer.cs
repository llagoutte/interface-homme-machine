﻿namespace GestionCourseOrientation
{
    partial class ConfigurationBalises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Grille = new System.Windows.Forms.DataGridView();
            this.BouttonAjouter = new System.Windows.Forms.Button();
            this.ColonneNumero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneIdentifiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneGPS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneSupprimer = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColonneTelecharger = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).BeginInit();
            this.SuspendLayout();
            // 
            // Grille
            // 
            this.Grille.AllowUserToAddRows = false;
            this.Grille.AllowUserToDeleteRows = false;
            this.Grille.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grille.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColonneNumero,
            this.ColonneIdentifiant,
            this.ColonneGPS,
            this.ColonneSupprimer,
            this.ColonneTelecharger});
            this.Grille.Location = new System.Drawing.Point(12, 12);
            this.Grille.Name = "Grille";
            this.Grille.ReadOnly = true;
            this.Grille.Size = new System.Drawing.Size(469, 215);
            this.Grille.TabIndex = 0;
            this.Grille.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grille_CellContentClick);
            // 
            // BouttonAjouter
            // 
            this.BouttonAjouter.Location = new System.Drawing.Point(193, 268);
            this.BouttonAjouter.Name = "BouttonAjouter";
            this.BouttonAjouter.Size = new System.Drawing.Size(101, 43);
            this.BouttonAjouter.TabIndex = 1;
            this.BouttonAjouter.Text = "Ajouter";
            this.BouttonAjouter.UseVisualStyleBackColor = true;
            this.BouttonAjouter.Click += new System.EventHandler(this.BouttonAjouter_Click);
            // 
            // ColonneNumero
            // 
            this.ColonneNumero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneNumero.HeaderText = "Numéro";
            this.ColonneNumero.Name = "ColonneNumero";
            this.ColonneNumero.ReadOnly = true;
            this.ColonneNumero.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneNumero.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColonneNumero.Width = 50;
            // 
            // ColonneIdentifiant
            // 
            this.ColonneIdentifiant.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneIdentifiant.HeaderText = "Identifiant";
            this.ColonneIdentifiant.Name = "ColonneIdentifiant";
            this.ColonneIdentifiant.ReadOnly = true;
            this.ColonneIdentifiant.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneIdentifiant.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColonneIdentifiant.Width = 59;
            // 
            // ColonneGPS
            // 
            this.ColonneGPS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneGPS.HeaderText = "GPS";
            this.ColonneGPS.Name = "ColonneGPS";
            this.ColonneGPS.ReadOnly = true;
            this.ColonneGPS.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneGPS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColonneGPS.Width = 35;
            // 
            // ColonneSupprimer
            // 
            this.ColonneSupprimer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneSupprimer.HeaderText = "Suppression";
            this.ColonneSupprimer.Name = "ColonneSupprimer";
            this.ColonneSupprimer.ReadOnly = true;
            this.ColonneSupprimer.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColonneSupprimer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColonneSupprimer.Width = 90;
            // 
            // ColonneTelecharger
            // 
            this.ColonneTelecharger.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneTelecharger.HeaderText = "Télécharger";
            this.ColonneTelecharger.Name = "ColonneTelecharger";
            this.ColonneTelecharger.ReadOnly = true;
            this.ColonneTelecharger.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneTelecharger.Width = 70;
            // 
            // ConfigurationBalises
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 366);
            this.Controls.Add(this.BouttonAjouter);
            this.Controls.Add(this.Grille);
            this.Name = "ConfigurationBalises";
            this.Text = "ConfigurationBalises";
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Grille;
        private System.Windows.Forms.Button BouttonAjouter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneIdentifiant;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneGPS;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneSupprimer;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneTelecharger;
    }
}