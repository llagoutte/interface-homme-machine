﻿namespace GestionCourseOrientation
{
    partial class ConfigurationCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBoxInfosCourse = new System.Windows.Forms.GroupBox();
            this.LabelLieu = new System.Windows.Forms.Label();
            this.LabelDate = new System.Windows.Forms.Label();
            this.LabelTypeCourse = new System.Windows.Forms.Label();
            this.LabelNomOrganisateur = new System.Windows.Forms.Label();
            this.LabelNomCourse = new System.Windows.Forms.Label();
            this.BouttonModifier = new System.Windows.Forms.Button();
            this.BouttonConfigurationBalise = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.BoutonParticipants = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.GroupBoxInfosCourse.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBoxInfosCourse
            // 
            this.GroupBoxInfosCourse.Controls.Add(this.LabelLieu);
            this.GroupBoxInfosCourse.Controls.Add(this.LabelDate);
            this.GroupBoxInfosCourse.Controls.Add(this.LabelTypeCourse);
            this.GroupBoxInfosCourse.Controls.Add(this.LabelNomOrganisateur);
            this.GroupBoxInfosCourse.Controls.Add(this.LabelNomCourse);
            this.GroupBoxInfosCourse.Location = new System.Drawing.Point(12, 12);
            this.GroupBoxInfosCourse.Name = "GroupBoxInfosCourse";
            this.GroupBoxInfosCourse.Size = new System.Drawing.Size(437, 197);
            this.GroupBoxInfosCourse.TabIndex = 0;
            this.GroupBoxInfosCourse.TabStop = false;
            this.GroupBoxInfosCourse.Text = "Infos Course";
            // 
            // LabelLieu
            // 
            this.LabelLieu.AutoSize = true;
            this.LabelLieu.Location = new System.Drawing.Point(18, 154);
            this.LabelLieu.Name = "LabelLieu";
            this.LabelLieu.Size = new System.Drawing.Size(36, 13);
            this.LabelLieu.TabIndex = 4;
            this.LabelLieu.Text = "Lieu  :";
            // 
            // LabelDate
            // 
            this.LabelDate.AutoSize = true;
            this.LabelDate.Location = new System.Drawing.Point(17, 123);
            this.LabelDate.Name = "LabelDate";
            this.LabelDate.Size = new System.Drawing.Size(39, 13);
            this.LabelDate.TabIndex = 3;
            this.LabelDate.Text = "Date  :";
            // 
            // LabelTypeCourse
            // 
            this.LabelTypeCourse.AutoSize = true;
            this.LabelTypeCourse.Location = new System.Drawing.Point(17, 93);
            this.LabelTypeCourse.Name = "LabelTypeCourse";
            this.LabelTypeCourse.Size = new System.Drawing.Size(40, 13);
            this.LabelTypeCourse.TabIndex = 2;
            this.LabelTypeCourse.Text = "Type  :";
            // 
            // LabelNomOrganisateur
            // 
            this.LabelNomOrganisateur.AutoSize = true;
            this.LabelNomOrganisateur.Location = new System.Drawing.Point(17, 64);
            this.LabelNomOrganisateur.Name = "LabelNomOrganisateur";
            this.LabelNomOrganisateur.Size = new System.Drawing.Size(101, 13);
            this.LabelNomOrganisateur.TabIndex = 1;
            this.LabelNomOrganisateur.Text = "Nom Organisateur  :";
            // 
            // LabelNomCourse
            // 
            this.LabelNomCourse.AutoSize = true;
            this.LabelNomCourse.Location = new System.Drawing.Point(17, 34);
            this.LabelNomCourse.Name = "LabelNomCourse";
            this.LabelNomCourse.Size = new System.Drawing.Size(35, 13);
            this.LabelNomCourse.TabIndex = 0;
            this.LabelNomCourse.Text = "Nom :";
            // 
            // BouttonModifier
            // 
            this.BouttonModifier.Location = new System.Drawing.Point(455, 92);
            this.BouttonModifier.Name = "BouttonModifier";
            this.BouttonModifier.Size = new System.Drawing.Size(96, 38);
            this.BouttonModifier.TabIndex = 1;
            this.BouttonModifier.Text = "Modifier";
            this.BouttonModifier.UseVisualStyleBackColor = true;
            this.BouttonModifier.Click += new System.EventHandler(this.BouttonModifier_Click);
            // 
            // BouttonConfigurationBalise
            // 
            this.BouttonConfigurationBalise.Location = new System.Drawing.Point(34, 262);
            this.BouttonConfigurationBalise.Name = "BouttonConfigurationBalise";
            this.BouttonConfigurationBalise.Size = new System.Drawing.Size(109, 51);
            this.BouttonConfigurationBalise.TabIndex = 2;
            this.BouttonConfigurationBalise.Text = "Configuration\r\nBalises";
            this.BouttonConfigurationBalise.UseVisualStyleBackColor = true;
            this.BouttonConfigurationBalise.Click += new System.EventHandler(this.BouttonConfigurationBalise_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(219, 262);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.button1.Size = new System.Drawing.Size(109, 51);
            this.button1.TabIndex = 3;
            this.button1.Text = "Définir\r\nCircuit";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // BoutonParticipants
            // 
            this.BoutonParticipants.Location = new System.Drawing.Point(420, 262);
            this.BoutonParticipants.Name = "BoutonParticipants";
            this.BoutonParticipants.Size = new System.Drawing.Size(109, 51);
            this.BoutonParticipants.TabIndex = 4;
            this.BoutonParticipants.Text = "Participants";
            this.BoutonParticipants.UseVisualStyleBackColor = true;
            this.BoutonParticipants.Click += new System.EventHandler(this.BoutonParticipants_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(128, 369);
            this.button2.Name = "button2";
            this.button2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.button2.Size = new System.Drawing.Size(110, 51);
            this.button2.TabIndex = 5;
            this.button2.Text = "Valider\r\nParticipant";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(323, 369);
            this.button3.Name = "button3";
            this.button3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.button3.Size = new System.Drawing.Size(109, 51);
            this.button3.TabIndex = 6;
            this.button3.Text = "Éditer";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // ConfigurationCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 526);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.BoutonParticipants);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BouttonConfigurationBalise);
            this.Controls.Add(this.BouttonModifier);
            this.Controls.Add(this.GroupBoxInfosCourse);
            this.Name = "ConfigurationCourse";
            this.Text = "ConfigurationCourse";
            this.Load += new System.EventHandler(this.ConfigurationCourse_Load);
            this.GroupBoxInfosCourse.ResumeLayout(false);
            this.GroupBoxInfosCourse.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBoxInfosCourse;
        private System.Windows.Forms.Label LabelLieu;
        private System.Windows.Forms.Label LabelDate;
        private System.Windows.Forms.Label LabelTypeCourse;
        private System.Windows.Forms.Label LabelNomOrganisateur;
        private System.Windows.Forms.Label LabelNomCourse;
        private System.Windows.Forms.Button BouttonModifier;
        private System.Windows.Forms.Button BouttonConfigurationBalise;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BoutonParticipants;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}