﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation.Courses.Configurations.Participants
{
    public partial class AjoutManuel : Form
    {
        private Participant _Participant;
        public AjoutManuel()
        {
            InitializeComponent();
            _Participant = new Participant();
        }

        public Participant GetParticipant{
            get { return _Participant; }
        }

        private void BoutonValider_Click(object sender, EventArgs e)
        {
            if(TextBoxNom.Text.Length != 0 && TextBoxPrenom.Text.Length != 0 && TextBoxDossard.Text.Length != 0)
            {
                _Participant.NomCoureur = TextBoxNom.Text;
                _Participant.PrenomCoureur = TextBoxPrenom.Text;
                _Participant.License = TextBoxLicense.Text;
                _Participant.Dossard = TextBoxDossard.Text;
                _Participant.TagC = Participant.GetNewId();
                this.Close();
            }
            else
            {
                MessageBox.Show("Compléter tous les champs avant ! ");
            }
        }
    }
}
