﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionCourseOrientation;
using TagCoureur;

namespace GestionCourseOrientation.Courses.Configurations.Participants
{
    public partial class ConfigurationParticipants : Form
    {
        private List<Participant> Participants;
        private List<Coureur> Coureurs;
        public ConfigurationParticipants(ref List<Participant> O_Participants)
        {
            InitializeComponent();
            Participants = O_Participants;
            Coureurs = GestionCO.Annuaire();
            AfficherAnnuaire();
            AfficherParticipants();
        }

        private void AfficherAnnuaire()
        {
            bool AlReady,AucunCoureur;
            int Position = 0;
            AucunCoureur = true;
            GrilleAnnuaire.Rows.Clear();
            foreach (Coureur AfficherCoureur in Coureurs)
            {
                AlReady = false;
                
                foreach (Participant AfficherParticipant in Participants)
                {
                   if(AfficherCoureur.PrenomCoureur == AfficherParticipant.PrenomCoureur && AfficherCoureur.NomCoureur == AfficherParticipant.NomCoureur && AfficherCoureur.License == AfficherParticipant.License)
                   {
                       AlReady = true;
                   }
                }
                if (AlReady == false)
                {
                    AucunCoureur = false;
                    GrilleAnnuaire.Rows.Add(Position.ToString(), AfficherCoureur.NomCoureur, AfficherCoureur.PrenomCoureur, AfficherCoureur.License, "Ajouter");
                }
                Position++;
            }
            
            if(AucunCoureur == true)
            {
                GrilleAnnuaire.Visible = false;
                LabelAnnuaire.Visible = true;
            }
            else
            {
                GrilleAnnuaire.Visible = true;
                LabelAnnuaire.Visible = false;
            }
        }

        private void AfficherParticipants()
        {
            bool AucunParticipants = true;
            int Position = 0;
            GrilleParticipants.Rows.Clear();
            foreach (Participant AfficherParticipant in Participants)
            {
                AucunParticipants = false;
                GrilleParticipants.Rows.Add(Position.ToString(), AfficherParticipant.NomCoureur, AfficherParticipant.PrenomCoureur, AfficherParticipant.License, AfficherParticipant.Dossard, AfficherParticipant.TagC, "Télécharger", "Retirer");
                Position++;
            }


            if (AucunParticipants == true)
            {
                LabelParticipant.Visible = true;
            }
            else
            {
                LabelParticipant.Visible = false;
            }
        }

        private void GrilleAnnuaire_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)  // Bouton Ajouter (Pour ajouter un coureur à la liste des participants)
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow Ligne = GrilleAnnuaire.Rows[e.RowIndex];
                    int Pos = Int32.Parse(Ligne.Cells[0].Value.ToString());

                    Participant NouveauParticipant = new Participant();
                    NouveauParticipant.NomCoureur = Coureurs[Pos].NomCoureur;
                    NouveauParticipant.PrenomCoureur = Coureurs[Pos].PrenomCoureur;
                    NouveauParticipant.License = Coureurs[Pos].License;
                    NouveauParticipant.Dossard = "";
                    NouveauParticipant.TagC = Participant.GetNewId();
                    Participants.Add(NouveauParticipant);
                    AfficherParticipants();
                    AfficherAnnuaire();
                }
            }
        }

        //
        //
        //
        // Partie Test d'intégration
        //
        //
        private void GestionParticipant(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)    //On vérifie que la DataGrid n'est pas vide !
            {
            DataGridViewRow Ligne = GrilleParticipants.Rows[e.RowIndex];
            int Pos = Int32.Parse(Ligne.Cells[0].Value.ToString());

            if (e.ColumnIndex == 6)  // Appuie sur le bouton Télécharger.
            {
                bool verif = false;
                verif = TagC.initialiserTag(Participants[Pos].TagC); //Appel de la fonction de l'etudiant numéro 2.4

                if(verif == false)
                {
                    MessageBox.Show("Erreur TagC !");
                }
                else
                {
                    MessageBox.Show("OK TagC");
                }
                //Participants[e.RowIndex].TagC;        //Utile pour dossier de test, tester le retour de la fonction de Nicolas
            }

            if (e.ColumnIndex == 7)  // Clique sur le bouton Retirer un participants
            {
                    Participants.Remove(Participants[Pos]);
                    AfficherParticipants(); //appel de la fonction pour rafraichir la DataGrid des participants
                    AfficherAnnuaire(); //Appel de la fonction pour rafraichir la DataGrid de l'annuaire Coureur
            }
           }
        }

        //
        //
        //
        //
        //
        //
        //
        //
        //

        private void BoutonAjoutManuel_Click(object sender, EventArgs e)
        {
            AjoutManuel AjoutParticipant = new AjoutManuel();
            AjoutParticipant.ShowDialog();

            Participants.Add(AjoutParticipant.GetParticipant);
            AfficherParticipants();
        }
    }
}
