﻿namespace GestionCourseOrientation.Courses.Configurations.Participants
{
    partial class ConfigurationParticipants
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrilleParticipants = new System.Windows.Forms.DataGridView();
            this.ColonneIDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneNom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonnePrenom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneLicense = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneDossard = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneTagC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneTelechargement = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColonneRetirer = new System.Windows.Forms.DataGridViewButtonColumn();
            this.GrilleAnnuaire = new System.Windows.Forms.DataGridView();
            this.ColonneId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneAjouter = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BoutonAjoutManuel = new System.Windows.Forms.Button();
            this.LabelAnnuaire = new System.Windows.Forms.Label();
            this.LabelParticipant = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GrilleParticipants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrilleAnnuaire)).BeginInit();
            this.SuspendLayout();
            // 
            // GrilleParticipants
            // 
            this.GrilleParticipants.AllowUserToAddRows = false;
            this.GrilleParticipants.AllowUserToDeleteRows = false;
            this.GrilleParticipants.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrilleParticipants.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColonneIDD,
            this.ColonneNom,
            this.ColonnePrenom,
            this.ColonneLicense,
            this.ColonneDossard,
            this.ColonneTagC,
            this.ColonneTelechargement,
            this.ColonneRetirer});
            this.GrilleParticipants.Location = new System.Drawing.Point(12, 12);
            this.GrilleParticipants.Name = "GrilleParticipants";
            this.GrilleParticipants.ReadOnly = true;
            this.GrilleParticipants.Size = new System.Drawing.Size(518, 252);
            this.GrilleParticipants.TabIndex = 0;
            this.GrilleParticipants.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GestionParticipant);
            // 
            // ColonneIDD
            // 
            this.ColonneIDD.HeaderText = "ID";
            this.ColonneIDD.Name = "ColonneIDD";
            this.ColonneIDD.ReadOnly = true;
            this.ColonneIDD.Visible = false;
            // 
            // ColonneNom
            // 
            this.ColonneNom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneNom.HeaderText = "Nom";
            this.ColonneNom.Name = "ColonneNom";
            this.ColonneNom.ReadOnly = true;
            this.ColonneNom.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneNom.Width = 54;
            // 
            // ColonnePrenom
            // 
            this.ColonnePrenom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonnePrenom.HeaderText = "Prénom";
            this.ColonnePrenom.Name = "ColonnePrenom";
            this.ColonnePrenom.ReadOnly = true;
            this.ColonnePrenom.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonnePrenom.Width = 68;
            // 
            // ColonneLicense
            // 
            this.ColonneLicense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneLicense.HeaderText = "License";
            this.ColonneLicense.Name = "ColonneLicense";
            this.ColonneLicense.ReadOnly = true;
            this.ColonneLicense.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneLicense.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColonneLicense.Width = 50;
            // 
            // ColonneDossard
            // 
            this.ColonneDossard.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneDossard.HeaderText = "Dossard";
            this.ColonneDossard.Name = "ColonneDossard";
            this.ColonneDossard.ReadOnly = true;
            this.ColonneDossard.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneDossard.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColonneDossard.Width = 52;
            // 
            // ColonneTagC
            // 
            this.ColonneTagC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneTagC.HeaderText = "TagC";
            this.ColonneTagC.Name = "ColonneTagC";
            this.ColonneTagC.ReadOnly = true;
            this.ColonneTagC.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneTagC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColonneTagC.Width = 39;
            // 
            // ColonneTelechargement
            // 
            this.ColonneTelechargement.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneTelechargement.HeaderText = "Téléchargement";
            this.ColonneTelechargement.Name = "ColonneTelechargement";
            this.ColonneTelechargement.ReadOnly = true;
            this.ColonneTelechargement.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneTelechargement.Width = 90;
            // 
            // ColonneRetirer
            // 
            this.ColonneRetirer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneRetirer.HeaderText = "Retirer le Participant";
            this.ColonneRetirer.Name = "ColonneRetirer";
            this.ColonneRetirer.ReadOnly = true;
            this.ColonneRetirer.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneRetirer.Width = 97;
            // 
            // GrilleAnnuaire
            // 
            this.GrilleAnnuaire.AllowUserToAddRows = false;
            this.GrilleAnnuaire.AllowUserToDeleteRows = false;
            this.GrilleAnnuaire.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrilleAnnuaire.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColonneId,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.ColonneAjouter});
            this.GrilleAnnuaire.Location = new System.Drawing.Point(12, 407);
            this.GrilleAnnuaire.Name = "GrilleAnnuaire";
            this.GrilleAnnuaire.Size = new System.Drawing.Size(518, 230);
            this.GrilleAnnuaire.TabIndex = 1;
            this.GrilleAnnuaire.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrilleAnnuaire_CellContentClick);
            // 
            // ColonneId
            // 
            this.ColonneId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColonneId.HeaderText = "Id";
            this.ColonneId.Name = "ColonneId";
            this.ColonneId.Visible = false;
            this.ColonneId.Width = 5;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.HeaderText = "Prenom";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.HeaderText = "License";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 80;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 80;
            // 
            // ColonneAjouter
            // 
            this.ColonneAjouter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneAjouter.HeaderText = "Ajouter Participant";
            this.ColonneAjouter.MinimumWidth = 90;
            this.ColonneAjouter.Name = "ColonneAjouter";
            this.ColonneAjouter.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneAjouter.Width = 90;
            // 
            // BoutonAjoutManuel
            // 
            this.BoutonAjoutManuel.Location = new System.Drawing.Point(220, 312);
            this.BoutonAjoutManuel.Name = "BoutonAjoutManuel";
            this.BoutonAjoutManuel.Size = new System.Drawing.Size(107, 46);
            this.BoutonAjoutManuel.TabIndex = 2;
            this.BoutonAjoutManuel.Text = "Ajout\r\nmanuel";
            this.BoutonAjoutManuel.UseVisualStyleBackColor = true;
            this.BoutonAjoutManuel.Click += new System.EventHandler(this.BoutonAjoutManuel_Click);
            // 
            // LabelAnnuaire
            // 
            this.LabelAnnuaire.AutoSize = true;
            this.LabelAnnuaire.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAnnuaire.Location = new System.Drawing.Point(61, 463);
            this.LabelAnnuaire.Name = "LabelAnnuaire";
            this.LabelAnnuaire.Size = new System.Drawing.Size(434, 69);
            this.LabelAnnuaire.TabIndex = 3;
            this.LabelAnnuaire.Text = "L\'annuaire coureur n\'étant pas rempli, \r\nvous pouvez ajouter des participants \r\nu" +
    "niquement manuellement";
            // 
            // LabelParticipant
            // 
            this.LabelParticipant.AutoSize = true;
            this.LabelParticipant.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelParticipant.Location = new System.Drawing.Point(79, 107);
            this.LabelParticipant.Name = "LabelParticipant";
            this.LabelParticipant.Size = new System.Drawing.Size(388, 23);
            this.LabelParticipant.TabIndex = 4;
            this.LabelParticipant.Text = "Ajoutez votre premier Participant !";
            // 
            // ConfigurationParticipants
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 649);
            this.Controls.Add(this.LabelParticipant);
            this.Controls.Add(this.LabelAnnuaire);
            this.Controls.Add(this.BoutonAjoutManuel);
            this.Controls.Add(this.GrilleAnnuaire);
            this.Controls.Add(this.GrilleParticipants);
            this.Name = "ConfigurationParticipants";
            this.Text = "Participants";
            ((System.ComponentModel.ISupportInitialize)(this.GrilleParticipants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrilleAnnuaire)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GrilleParticipants;
        private System.Windows.Forms.DataGridView GrilleAnnuaire;
        private System.Windows.Forms.Button BoutonAjoutManuel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneAjouter;
        private System.Windows.Forms.Label LabelAnnuaire;
        private System.Windows.Forms.Label LabelParticipant;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneIDD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneNom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonnePrenom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneLicense;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneDossard;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneTagC;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneTelechargement;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneRetirer;
    }
}