﻿namespace GestionCourseOrientation.Courses.Configurations.Participants
{
    partial class AjoutManuel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxNom = new System.Windows.Forms.TextBox();
            this.TextBoxPrenom = new System.Windows.Forms.TextBox();
            this.TextBoxLicense = new System.Windows.Forms.TextBox();
            this.TextBoxDossard = new System.Windows.Forms.TextBox();
            this.BoutonValider = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prenom :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Dossard :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "License :";
            // 
            // TextBoxNom
            // 
            this.TextBoxNom.Location = new System.Drawing.Point(180, 43);
            this.TextBoxNom.Name = "TextBoxNom";
            this.TextBoxNom.Size = new System.Drawing.Size(180, 20);
            this.TextBoxNom.TabIndex = 4;
            // 
            // TextBoxPrenom
            // 
            this.TextBoxPrenom.Location = new System.Drawing.Point(180, 100);
            this.TextBoxPrenom.Name = "TextBoxPrenom";
            this.TextBoxPrenom.Size = new System.Drawing.Size(180, 20);
            this.TextBoxPrenom.TabIndex = 5;
            // 
            // TextBoxLicense
            // 
            this.TextBoxLicense.Location = new System.Drawing.Point(180, 157);
            this.TextBoxLicense.Name = "TextBoxLicense";
            this.TextBoxLicense.Size = new System.Drawing.Size(180, 20);
            this.TextBoxLicense.TabIndex = 6;
            // 
            // TextBoxDossard
            // 
            this.TextBoxDossard.Location = new System.Drawing.Point(180, 215);
            this.TextBoxDossard.Name = "TextBoxDossard";
            this.TextBoxDossard.Size = new System.Drawing.Size(180, 20);
            this.TextBoxDossard.TabIndex = 7;
            // 
            // BoutonValider
            // 
            this.BoutonValider.Location = new System.Drawing.Point(212, 257);
            this.BoutonValider.Name = "BoutonValider";
            this.BoutonValider.Size = new System.Drawing.Size(112, 39);
            this.BoutonValider.TabIndex = 8;
            this.BoutonValider.Text = "Valider";
            this.BoutonValider.UseVisualStyleBackColor = true;
            this.BoutonValider.Click += new System.EventHandler(this.BoutonValider_Click);
            // 
            // AjoutManuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 320);
            this.Controls.Add(this.BoutonValider);
            this.Controls.Add(this.TextBoxDossard);
            this.Controls.Add(this.TextBoxLicense);
            this.Controls.Add(this.TextBoxPrenom);
            this.Controls.Add(this.TextBoxNom);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AjoutManuel";
            this.Text = "AjoutManuel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBoxNom;
        private System.Windows.Forms.TextBox TextBoxPrenom;
        private System.Windows.Forms.TextBox TextBoxLicense;
        private System.Windows.Forms.TextBox TextBoxDossard;
        private System.Windows.Forms.Button BoutonValider;
    }
}