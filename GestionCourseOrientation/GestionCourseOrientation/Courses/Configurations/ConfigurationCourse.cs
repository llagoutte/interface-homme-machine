﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionCourseOrientation.Courses.Configurations.ConfigurationsBalises;

namespace GestionCourseOrientation
{
    public partial class ConfigurationCourse : Form
    {
        Course Course;
        public ConfigurationCourse(ref Course O_Course)
        {
            InitializeComponent();
            Course = O_Course;
        }

        private void ConfigurationCourse_Load(object sender, EventArgs e)
        {
            this.RafraichirInfosCourse();
        }

        private void BouttonModifier_Click(object sender, EventArgs e)
        {
            NouvelleCourse ModifierCourse = new NouvelleCourse(ref Course);
            ModifierCourse.ShowDialog();
            this.RafraichirInfosCourse();
        }

        private void RafraichirInfosCourse()
        {
            LabelNomCourse.Text = "Nom : " + Course.NomCourse;
            LabelNomOrganisateur.Text = "Nom Organisateur : " + Course.NomOrganisateur;
            LabelTypeCourse.Text = "Type : " + Course.TypeCourse;
            LabelDate.Text = "Date : " + Course.Date;
            LabelLieu.Text = "Lieu : " + Course.Lieu;
        }

        private void BouttonConfigurationBalise_Click(object sender, EventArgs e)
        {
            Course.ConfigurerBalises();
        }

        private void BoutonParticipants_Click(object sender, EventArgs e)
        {
            Course.ConfigurerParticipants();
        }
    }
}
