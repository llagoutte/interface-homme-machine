﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation
{
    public partial class NouvelleCourse : Form
    {
        private Course Course;
        private bool ChampsValide;
        public NouvelleCourse(ref Course R_Course)
        {
            InitializeComponent();
            Course = R_Course;
            ChampsValide = false;
            ComboBoxTypeCourse.DataSource = new[] { "Course", "Entraînement"};
        }

        public bool _ChampsValide
        {
            get { return ChampsValide; }
        }

        private void BoutonEnregistrer_Click(object sender, EventArgs e)
        {
            if(TextBoxNomCourse.Text.Length == 0 || TextBoxNomOrganisateur.Text.Length == 0 || TextBoxLieu.Text.Length == 0)
            {
                MessageBox.Show("Veuillez remplir les champs ! ");
            }
            else
            {
                Course.NomCourse = TextBoxNomCourse.Text;
                Course.NomOrganisateur = TextBoxNomOrganisateur.Text;
                Course.TypeCourse = ComboBoxTypeCourse.Text;
                Course.Date = DatePicker.Text;
                Course.Lieu = TextBoxLieu.Text;
                ChampsValide = true;
                this.Close();
            }
        }

        private void NouvelleCourse_Load(object sender, EventArgs e)
        {
                TextBoxNomCourse.Text = Course.NomCourse;
                TextBoxNomOrganisateur.Text = Course.NomOrganisateur;
                ComboBoxTypeCourse.Text = Course.TypeCourse;
                DatePicker.Text = Course.Date;
                TextBoxLieu.Text = Course.Lieu;
        }
    }
}
