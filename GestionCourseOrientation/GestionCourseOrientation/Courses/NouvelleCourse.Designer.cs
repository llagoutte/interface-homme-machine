﻿namespace GestionCourseOrientation
{
    partial class NouvelleCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelNouvelleCourse = new System.Windows.Forms.Label();
            this.LabelNomCourse = new System.Windows.Forms.Label();
            this.LabelNomOrganisateur = new System.Windows.Forms.Label();
            this.LabelTypeCourse = new System.Windows.Forms.Label();
            this.LabelDate = new System.Windows.Forms.Label();
            this.LabelLieu = new System.Windows.Forms.Label();
            this.TextBoxNomCourse = new System.Windows.Forms.TextBox();
            this.TextBoxNomOrganisateur = new System.Windows.Forms.TextBox();
            this.ComboBoxTypeCourse = new System.Windows.Forms.ComboBox();
            this.DatePicker = new System.Windows.Forms.DateTimePicker();
            this.TextBoxLieu = new System.Windows.Forms.TextBox();
            this.BoutonEnregistrer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelNouvelleCourse
            // 
            this.LabelNouvelleCourse.AutoSize = true;
            this.LabelNouvelleCourse.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNouvelleCourse.Location = new System.Drawing.Point(148, 9);
            this.LabelNouvelleCourse.Name = "LabelNouvelleCourse";
            this.LabelNouvelleCourse.Size = new System.Drawing.Size(262, 32);
            this.LabelNouvelleCourse.TabIndex = 0;
            this.LabelNouvelleCourse.Text = "Nouvelle Course";
            // 
            // LabelNomCourse
            // 
            this.LabelNomCourse.AutoSize = true;
            this.LabelNomCourse.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNomCourse.Location = new System.Drawing.Point(26, 100);
            this.LabelNomCourse.Name = "LabelNomCourse";
            this.LabelNomCourse.Size = new System.Drawing.Size(194, 23);
            this.LabelNomCourse.TabIndex = 1;
            this.LabelNomCourse.Text = "Nom de la course :";
            // 
            // LabelNomOrganisateur
            // 
            this.LabelNomOrganisateur.AutoSize = true;
            this.LabelNomOrganisateur.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNomOrganisateur.Location = new System.Drawing.Point(26, 179);
            this.LabelNomOrganisateur.Name = "LabelNomOrganisateur";
            this.LabelNomOrganisateur.Size = new System.Drawing.Size(238, 23);
            this.LabelNomOrganisateur.TabIndex = 2;
            this.LabelNomOrganisateur.Text = "Nom de l\'organisateur :";
            // 
            // LabelTypeCourse
            // 
            this.LabelTypeCourse.AutoSize = true;
            this.LabelTypeCourse.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTypeCourse.Location = new System.Drawing.Point(26, 257);
            this.LabelTypeCourse.Name = "LabelTypeCourse";
            this.LabelTypeCourse.Size = new System.Drawing.Size(196, 23);
            this.LabelTypeCourse.TabIndex = 3;
            this.LabelTypeCourse.Text = "Type de la course :";
            // 
            // LabelDate
            // 
            this.LabelDate.AutoSize = true;
            this.LabelDate.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDate.Location = new System.Drawing.Point(26, 328);
            this.LabelDate.Name = "LabelDate";
            this.LabelDate.Size = new System.Drawing.Size(70, 23);
            this.LabelDate.TabIndex = 4;
            this.LabelDate.Text = "Date :";
            // 
            // LabelLieu
            // 
            this.LabelLieu.AutoSize = true;
            this.LabelLieu.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLieu.Location = new System.Drawing.Point(26, 395);
            this.LabelLieu.Name = "LabelLieu";
            this.LabelLieu.Size = new System.Drawing.Size(66, 23);
            this.LabelLieu.TabIndex = 5;
            this.LabelLieu.Text = "Lieu :";
            // 
            // TextBoxNomCourse
            // 
            this.TextBoxNomCourse.Location = new System.Drawing.Point(320, 103);
            this.TextBoxNomCourse.Name = "TextBoxNomCourse";
            this.TextBoxNomCourse.Size = new System.Drawing.Size(203, 20);
            this.TextBoxNomCourse.TabIndex = 6;
            // 
            // TextBoxNomOrganisateur
            // 
            this.TextBoxNomOrganisateur.Location = new System.Drawing.Point(320, 182);
            this.TextBoxNomOrganisateur.Name = "TextBoxNomOrganisateur";
            this.TextBoxNomOrganisateur.Size = new System.Drawing.Size(203, 20);
            this.TextBoxNomOrganisateur.TabIndex = 7;
            // 
            // ComboBoxTypeCourse
            // 
            this.ComboBoxTypeCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTypeCourse.FormattingEnabled = true;
            this.ComboBoxTypeCourse.Location = new System.Drawing.Point(320, 258);
            this.ComboBoxTypeCourse.Name = "ComboBoxTypeCourse";
            this.ComboBoxTypeCourse.Size = new System.Drawing.Size(203, 21);
            this.ComboBoxTypeCourse.TabIndex = 8;
            // 
            // DatePicker
            // 
            this.DatePicker.Location = new System.Drawing.Point(320, 330);
            this.DatePicker.Name = "DatePicker";
            this.DatePicker.Size = new System.Drawing.Size(200, 20);
            this.DatePicker.TabIndex = 9;
            // 
            // TextBoxLieu
            // 
            this.TextBoxLieu.Location = new System.Drawing.Point(320, 398);
            this.TextBoxLieu.Name = "TextBoxLieu";
            this.TextBoxLieu.Size = new System.Drawing.Size(203, 20);
            this.TextBoxLieu.TabIndex = 10;
            // 
            // BoutonEnregistrer
            // 
            this.BoutonEnregistrer.Location = new System.Drawing.Point(251, 477);
            this.BoutonEnregistrer.Name = "BoutonEnregistrer";
            this.BoutonEnregistrer.Size = new System.Drawing.Size(102, 33);
            this.BoutonEnregistrer.TabIndex = 11;
            this.BoutonEnregistrer.Text = "Enregistrer";
            this.BoutonEnregistrer.UseVisualStyleBackColor = true;
            this.BoutonEnregistrer.Click += new System.EventHandler(this.BoutonEnregistrer_Click);
            // 
            // NouvelleCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 522);
            this.Controls.Add(this.BoutonEnregistrer);
            this.Controls.Add(this.TextBoxLieu);
            this.Controls.Add(this.DatePicker);
            this.Controls.Add(this.ComboBoxTypeCourse);
            this.Controls.Add(this.TextBoxNomOrganisateur);
            this.Controls.Add(this.TextBoxNomCourse);
            this.Controls.Add(this.LabelLieu);
            this.Controls.Add(this.LabelDate);
            this.Controls.Add(this.LabelTypeCourse);
            this.Controls.Add(this.LabelNomOrganisateur);
            this.Controls.Add(this.LabelNomCourse);
            this.Controls.Add(this.LabelNouvelleCourse);
            this.Name = "NouvelleCourse";
            this.Text = "NouvelleCourse";
            this.Load += new System.EventHandler(this.NouvelleCourse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelNouvelleCourse;
        private System.Windows.Forms.Label LabelNomCourse;
        private System.Windows.Forms.Label LabelNomOrganisateur;
        private System.Windows.Forms.Label LabelTypeCourse;
        private System.Windows.Forms.Label LabelDate;
        private System.Windows.Forms.Label LabelLieu;
        private System.Windows.Forms.TextBox TextBoxNomCourse;
        private System.Windows.Forms.TextBox TextBoxNomOrganisateur;
        private System.Windows.Forms.ComboBox ComboBoxTypeCourse;
        private System.Windows.Forms.DateTimePicker DatePicker;
        private System.Windows.Forms.TextBox TextBoxLieu;
        private System.Windows.Forms.Button BoutonEnregistrer;
    }
}