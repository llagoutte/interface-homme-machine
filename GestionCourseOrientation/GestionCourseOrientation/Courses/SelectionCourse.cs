﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation
{
    public partial class SelectionCourse : Form
    {
        private List<Course> Course;
        private int _Index;

        public SelectionCourse(List<Course> L_Course)
        {
            InitializeComponent();
            Course = L_Course;
            _Index = -1;
        }

        private void SelectionCourse_Load(object sender, EventArgs e)
        {
            int Position = 0;
            foreach (Course AfficherCourse in Course)
            {
                Grille.Rows.Add(Position.ToString() ,AfficherCourse.NomCourse, AfficherCourse.Date, AfficherCourse.Lieu, "Sélectionné");
                Position++;
            }
        }

        private void Grille_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 4)
                {
                    DataGridViewRow Ligne = Grille.Rows[e.RowIndex];
                    int Pos = Int32.Parse(Ligne.Cells[0].Value.ToString());

                    _Index = Pos;
                    this.Close();
                }
            }
        }

        public int Index
        {
            set { _Index = value; }
            get { return _Index; }
        }

    }
}
