﻿namespace GestionCourseOrientation
{
    partial class SelectionCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Grille = new System.Windows.Forms.DataGridView();
            this.ColonneId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneNomCourse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneLieu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneBouton = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).BeginInit();
            this.SuspendLayout();
            // 
            // Grille
            // 
            this.Grille.AllowUserToAddRows = false;
            this.Grille.AllowUserToDeleteRows = false;
            this.Grille.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grille.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColonneId,
            this.ColonneNomCourse,
            this.ColonneDate,
            this.ColonneLieu,
            this.ColonneBouton});
            this.Grille.Location = new System.Drawing.Point(12, 29);
            this.Grille.Name = "Grille";
            this.Grille.ReadOnly = true;
            this.Grille.Size = new System.Drawing.Size(539, 256);
            this.Grille.TabIndex = 0;
            this.Grille.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grille_CellContentClick);
            // 
            // ColonneId
            // 
            this.ColonneId.HeaderText = "ID";
            this.ColonneId.Name = "ColonneId";
            this.ColonneId.ReadOnly = true;
            this.ColonneId.Visible = false;
            // 
            // ColonneNomCourse
            // 
            this.ColonneNomCourse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneNomCourse.HeaderText = "Nom de la course";
            this.ColonneNomCourse.Name = "ColonneNomCourse";
            this.ColonneNomCourse.ReadOnly = true;
            this.ColonneNomCourse.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneNomCourse.Width = 77;
            // 
            // ColonneDate
            // 
            this.ColonneDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneDate.HeaderText = "Date";
            this.ColonneDate.Name = "ColonneDate";
            this.ColonneDate.ReadOnly = true;
            this.ColonneDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneDate.Width = 55;
            // 
            // ColonneLieu
            // 
            this.ColonneLieu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneLieu.HeaderText = "Lieu";
            this.ColonneLieu.Name = "ColonneLieu";
            this.ColonneLieu.ReadOnly = true;
            this.ColonneLieu.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneLieu.Width = 52;
            // 
            // ColonneBouton
            // 
            this.ColonneBouton.HeaderText = "";
            this.ColonneBouton.Name = "ColonneBouton";
            this.ColonneBouton.ReadOnly = true;
            this.ColonneBouton.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // SelectionCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 498);
            this.Controls.Add(this.Grille);
            this.Name = "SelectionCourse";
            this.Text = "SelectionCourse";
            this.Load += new System.EventHandler(this.SelectionCourse_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Grille;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneNomCourse;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneLieu;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneBouton;
    }
}