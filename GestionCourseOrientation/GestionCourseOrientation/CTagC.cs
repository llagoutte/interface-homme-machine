﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// ajout de la référence 'OneWireLinkLayer' au projet
using DalSemi.OneWire.Adapter;
using DalSemi.OneWire;

namespace TagCoureur
{
    class TagC
    {

        public static bool initialiserTag(int idCoureur)
        {
            PortAdapter adapter = null;
            try
            {
                // si l'id coureur est trop grand ou trop petit, met fin à l'initialisation
                if (idCoureur > 100 || idCoureur < 0) return false;

                adapter = AccessProvider.GetAdapter("{DS9490}", "USB1");
                CTag tag = new CTag();

                int family = 6, address = 0x0;
                string tagMem = "";
                bool retourInit = false, retourRead = false;
                int cpt = 0;

                // contrôle exclusif de l'adaptateur
                adapter.BeginExclusive(true);
                // initialisation du tag
                retourInit = tag.initTag(ref adapter, family, idCoureur);
                // vérification de l'initialisation
                retourRead = tag.readTag(ref adapter, address, ref tagMem, ref cpt);

                // libère le contrôle exclusif de l'adaptateur, les ressources et le port
                adapter.EndExclusive();
                adapter.Dispose();
                adapter.FreePort();

                if (!retourInit || retourRead) return false;
                else return true;
            }
            catch (AdapterException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

        /*public static string verifierTag(ref string parcours)
        {
            PortAdapter adapter = null;
            try
            {
                adapter = AccessProvider.GetAdapter("{DS9490}", "USB1");
                CTag tag = new CTag();

                int address = 0x0;
                bool retourCheck = false;

                // contrôle exclusif de l'adaptateur
                adapter.BeginExclusive(true);

                // vérifie le contenu du tag avec le parcours de la base de données
                retourCheck = tag.checkTag(ref adapter, address, ref parcours);

                // libère le contrôle exclusif de l'adaptateur, les ressources et le port
                adapter.EndExclusive();
                adapter.Dispose();
                adapter.FreePort();

                if (retourCheck) return "Parcours validé !";
                else return "Parcours mauvais !";
            }
            catch (AdapterException ex)
            {
                //MessageBox.Show(ex.Message);
                return "Erreur !";
            }
        }*/
    }
}
