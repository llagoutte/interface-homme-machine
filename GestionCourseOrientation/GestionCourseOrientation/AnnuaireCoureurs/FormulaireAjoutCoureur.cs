﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation
{
    public partial class FormulaireAjoutCoureur : Form
    {
        private Coureur _Coureur;
        private bool _ChampsValide;
        public FormulaireAjoutCoureur()
        {
            InitializeComponent();
            _Coureur = new Coureur();
            _ChampsValide = false;
        }

        public bool ChampsValide
        {
            get { return _ChampsValide; }
        }

        private void BouttonAjouter_Click(object sender, EventArgs e)
        {
            if(TextBoxNom.Text != "" && TextBoxPrenom.Text != "" && TextBoxLicense.Text != "")
            {
                _Coureur.NomCoureur = TextBoxNom.Text;
                _Coureur.PrenomCoureur = TextBoxPrenom.Text;
                _Coureur.License = TextBoxLicense.Text;
                _ChampsValide = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Veuillez remplir tous les champs !");
            }
        }

        public Coureur Coureur
        {
            get { return _Coureur; }
        }
    }
}
