﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation
{
    public partial class ModificationCoureur : Form
    {
        private Coureur _Coureur;
        public ModificationCoureur(Coureur O_Coureur)
        {
            InitializeComponent();
            _Coureur = O_Coureur;
            TextBoxNom.Text = _Coureur.NomCoureur;
            TextBoxPrenom.Text = _Coureur.PrenomCoureur;
            TextBoxLicense.Text = _Coureur.License;
        }

        public Coureur Coureur
        {
            get { return _Coureur; }
        }

        private void BoutonValider_Click(object sender, EventArgs e)
        {
            _Coureur.NomCoureur = TextBoxNom.Text;
            _Coureur.PrenomCoureur = TextBoxPrenom.Text;
            _Coureur.License = TextBoxLicense.Text;
            this.Close();
        }
    }
}
