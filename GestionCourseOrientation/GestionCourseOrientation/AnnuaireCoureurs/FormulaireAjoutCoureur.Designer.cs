﻿namespace GestionCourseOrientation
{
    partial class FormulaireAjoutCoureur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelNom = new System.Windows.Forms.Label();
            this.LabelPrenom = new System.Windows.Forms.Label();
            this.LabelLicense = new System.Windows.Forms.Label();
            this.TextBoxNom = new System.Windows.Forms.TextBox();
            this.TextBoxPrenom = new System.Windows.Forms.TextBox();
            this.TextBoxLicense = new System.Windows.Forms.TextBox();
            this.BouttonAjouter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelNom
            // 
            this.LabelNom.AutoSize = true;
            this.LabelNom.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNom.Location = new System.Drawing.Point(35, 77);
            this.LabelNom.Name = "LabelNom";
            this.LabelNom.Size = new System.Drawing.Size(58, 18);
            this.LabelNom.TabIndex = 0;
            this.LabelNom.Text = "Nom :";
            // 
            // LabelPrenom
            // 
            this.LabelPrenom.AutoSize = true;
            this.LabelPrenom.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPrenom.Location = new System.Drawing.Point(35, 132);
            this.LabelPrenom.Name = "LabelPrenom";
            this.LabelPrenom.Size = new System.Drawing.Size(82, 18);
            this.LabelPrenom.TabIndex = 1;
            this.LabelPrenom.Text = "Prenom :";
            // 
            // LabelLicense
            // 
            this.LabelLicense.AutoSize = true;
            this.LabelLicense.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLicense.Location = new System.Drawing.Point(35, 185);
            this.LabelLicense.Name = "LabelLicense";
            this.LabelLicense.Size = new System.Drawing.Size(82, 18);
            this.LabelLicense.TabIndex = 2;
            this.LabelLicense.Text = "License :";
            // 
            // TextBoxNom
            // 
            this.TextBoxNom.Location = new System.Drawing.Point(164, 75);
            this.TextBoxNom.Name = "TextBoxNom";
            this.TextBoxNom.Size = new System.Drawing.Size(200, 20);
            this.TextBoxNom.TabIndex = 3;
            // 
            // TextBoxPrenom
            // 
            this.TextBoxPrenom.Location = new System.Drawing.Point(164, 130);
            this.TextBoxPrenom.Name = "TextBoxPrenom";
            this.TextBoxPrenom.Size = new System.Drawing.Size(200, 20);
            this.TextBoxPrenom.TabIndex = 4;
            // 
            // TextBoxLicense
            // 
            this.TextBoxLicense.Location = new System.Drawing.Point(164, 183);
            this.TextBoxLicense.Name = "TextBoxLicense";
            this.TextBoxLicense.Size = new System.Drawing.Size(200, 20);
            this.TextBoxLicense.TabIndex = 5;
            // 
            // BouttonAjouter
            // 
            this.BouttonAjouter.Location = new System.Drawing.Point(246, 286);
            this.BouttonAjouter.Name = "BouttonAjouter";
            this.BouttonAjouter.Size = new System.Drawing.Size(93, 42);
            this.BouttonAjouter.TabIndex = 6;
            this.BouttonAjouter.Text = "Ajouter";
            this.BouttonAjouter.UseVisualStyleBackColor = true;
            this.BouttonAjouter.Click += new System.EventHandler(this.BouttonAjouter_Click);
            // 
            // FormulaireAjoutCoureur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 370);
            this.Controls.Add(this.BouttonAjouter);
            this.Controls.Add(this.TextBoxLicense);
            this.Controls.Add(this.TextBoxPrenom);
            this.Controls.Add(this.TextBoxNom);
            this.Controls.Add(this.LabelLicense);
            this.Controls.Add(this.LabelPrenom);
            this.Controls.Add(this.LabelNom);
            this.Name = "FormulaireAjoutCoureur";
            this.Text = "FormulaireAjoutCoureur";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelNom;
        private System.Windows.Forms.Label LabelPrenom;
        private System.Windows.Forms.Label LabelLicense;
        private System.Windows.Forms.TextBox TextBoxNom;
        private System.Windows.Forms.TextBox TextBoxPrenom;
        private System.Windows.Forms.TextBox TextBoxLicense;
        private System.Windows.Forms.Button BouttonAjouter;
    }
}