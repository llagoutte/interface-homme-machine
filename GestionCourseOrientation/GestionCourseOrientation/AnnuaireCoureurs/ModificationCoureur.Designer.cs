﻿namespace GestionCourseOrientation
{
    partial class ModificationCoureur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelNom = new System.Windows.Forms.Label();
            this.LabelPrenom = new System.Windows.Forms.Label();
            this.LabelLicense = new System.Windows.Forms.Label();
            this.BoutonValider = new System.Windows.Forms.Button();
            this.TextBoxNom = new System.Windows.Forms.TextBox();
            this.TextBoxPrenom = new System.Windows.Forms.TextBox();
            this.TextBoxLicense = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LabelNom
            // 
            this.LabelNom.AutoSize = true;
            this.LabelNom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LabelNom.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNom.Location = new System.Drawing.Point(27, 39);
            this.LabelNom.Name = "LabelNom";
            this.LabelNom.Size = new System.Drawing.Size(58, 36);
            this.LabelNom.TabIndex = 0;
            this.LabelNom.Text = "Nom :\r\n";
            // 
            // LabelPrenom
            // 
            this.LabelPrenom.AutoSize = true;
            this.LabelPrenom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LabelPrenom.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPrenom.Location = new System.Drawing.Point(27, 89);
            this.LabelPrenom.Name = "LabelPrenom";
            this.LabelPrenom.Size = new System.Drawing.Size(82, 18);
            this.LabelPrenom.TabIndex = 1;
            this.LabelPrenom.Text = "Prenom :\r\n";
            // 
            // LabelLicense
            // 
            this.LabelLicense.AutoSize = true;
            this.LabelLicense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LabelLicense.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLicense.Location = new System.Drawing.Point(27, 140);
            this.LabelLicense.Name = "LabelLicense";
            this.LabelLicense.Size = new System.Drawing.Size(82, 18);
            this.LabelLicense.TabIndex = 2;
            this.LabelLicense.Text = "License :\r\n";
            // 
            // BoutonValider
            // 
            this.BoutonValider.Location = new System.Drawing.Point(162, 217);
            this.BoutonValider.Name = "BoutonValider";
            this.BoutonValider.Size = new System.Drawing.Size(75, 23);
            this.BoutonValider.TabIndex = 3;
            this.BoutonValider.Text = "Valider";
            this.BoutonValider.UseVisualStyleBackColor = true;
            this.BoutonValider.Click += new System.EventHandler(this.BoutonValider_Click);
            // 
            // TextBoxNom
            // 
            this.TextBoxNom.Location = new System.Drawing.Point(162, 40);
            this.TextBoxNom.Name = "TextBoxNom";
            this.TextBoxNom.Size = new System.Drawing.Size(137, 20);
            this.TextBoxNom.TabIndex = 4;
            // 
            // TextBoxPrenom
            // 
            this.TextBoxPrenom.Location = new System.Drawing.Point(162, 90);
            this.TextBoxPrenom.Name = "TextBoxPrenom";
            this.TextBoxPrenom.Size = new System.Drawing.Size(137, 20);
            this.TextBoxPrenom.TabIndex = 5;
            // 
            // TextBoxLicense
            // 
            this.TextBoxLicense.Location = new System.Drawing.Point(162, 141);
            this.TextBoxLicense.Name = "TextBoxLicense";
            this.TextBoxLicense.Size = new System.Drawing.Size(137, 20);
            this.TextBoxLicense.TabIndex = 6;
            // 
            // ModificationCoureur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 275);
            this.Controls.Add(this.TextBoxLicense);
            this.Controls.Add(this.TextBoxPrenom);
            this.Controls.Add(this.TextBoxNom);
            this.Controls.Add(this.BoutonValider);
            this.Controls.Add(this.LabelLicense);
            this.Controls.Add(this.LabelPrenom);
            this.Controls.Add(this.LabelNom);
            this.Name = "ModificationCoureur";
            this.Text = "ModificationCoureur";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelNom;
        private System.Windows.Forms.Label LabelPrenom;
        private System.Windows.Forms.Label LabelLicense;
        private System.Windows.Forms.Button BoutonValider;
        private System.Windows.Forms.TextBox TextBoxNom;
        private System.Windows.Forms.TextBox TextBoxPrenom;
        private System.Windows.Forms.TextBox TextBoxLicense;
    }
}