﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation
{
    public partial class AnnuaireCoureurs : Form
    {
        private List<Coureur> Coureurs;
        public AnnuaireCoureurs(ref List<Coureur> L_Coureurs)
        {
            InitializeComponent();
            Coureurs = L_Coureurs;
            AfficherListe();
        }

        private void AfficherListe()
        {
            int Position = 0;
            Grille.Rows.Clear();
            foreach (Coureur AfficherCoureur in Coureurs)
            {
                Grille.Rows.Add(Position.ToString(), AfficherCoureur.NomCoureur, AfficherCoureur.PrenomCoureur, AfficherCoureur.License, "Modifier", "Supprimer");
                Position++;
            }
        }

        private void BouttonAjouter_Click(object sender, EventArgs e)
        {
            FormulaireAjoutCoureur AjoutCoureur = new FormulaireAjoutCoureur();
            AjoutCoureur.ShowDialog();
            if (AjoutCoureur.ChampsValide == true)
            {
                Coureurs.Add(AjoutCoureur.Coureur);
            }
            AfficherListe();
        }

        private void Grille_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow Ligne = Grille.Rows[e.RowIndex];
                int Pos = Int32.Parse(Ligne.Cells[0].Value.ToString());

                if (e.ColumnIndex == 4) // Modification d'un Coureur
                {
                    ModificationCoureur CoureurAModifier = new ModificationCoureur(Coureurs[Pos]);
                    CoureurAModifier.ShowDialog();
                    Coureurs[Pos] = CoureurAModifier.Coureur;
                    AfficherListe();
                }

                if (e.ColumnIndex == 5) // Suppression d'un Coureur
                {
                    Coureurs.Remove(Coureurs[Pos]);
                    AfficherListe();
                }
            }
        }
    }
}
