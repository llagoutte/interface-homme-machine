﻿namespace GestionCourseOrientation
{
    partial class AnnuaireCoureurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Grille = new System.Windows.Forms.DataGridView();
            this.BouttonAjouter = new System.Windows.Forms.Button();
            this.ColonneID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneNom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonnePrenom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneLicense = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColonneModification = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColonneSuppression = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).BeginInit();
            this.SuspendLayout();
            // 
            // Grille
            // 
            this.Grille.AllowUserToAddRows = false;
            this.Grille.AllowUserToDeleteRows = false;
            this.Grille.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grille.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColonneID,
            this.ColonneNom,
            this.ColonnePrenom,
            this.ColonneLicense,
            this.ColonneModification,
            this.ColonneSuppression});
            this.Grille.Location = new System.Drawing.Point(68, 23);
            this.Grille.Name = "Grille";
            this.Grille.ReadOnly = true;
            this.Grille.Size = new System.Drawing.Size(470, 230);
            this.Grille.TabIndex = 0;
            this.Grille.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grille_CellContentClick);
            // 
            // BouttonAjouter
            // 
            this.BouttonAjouter.Location = new System.Drawing.Point(246, 337);
            this.BouttonAjouter.Name = "BouttonAjouter";
            this.BouttonAjouter.Size = new System.Drawing.Size(115, 51);
            this.BouttonAjouter.TabIndex = 1;
            this.BouttonAjouter.Text = "Ajouter";
            this.BouttonAjouter.UseVisualStyleBackColor = true;
            this.BouttonAjouter.Click += new System.EventHandler(this.BouttonAjouter_Click);
            // 
            // ColonneID
            // 
            this.ColonneID.HeaderText = "ID";
            this.ColonneID.Name = "ColonneID";
            this.ColonneID.ReadOnly = true;
            this.ColonneID.Visible = false;
            // 
            // ColonneNom
            // 
            this.ColonneNom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneNom.HeaderText = "Nom";
            this.ColonneNom.Name = "ColonneNom";
            this.ColonneNom.ReadOnly = true;
            this.ColonneNom.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneNom.Width = 54;
            // 
            // ColonnePrenom
            // 
            this.ColonnePrenom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonnePrenom.HeaderText = "Prenom";
            this.ColonnePrenom.Name = "ColonnePrenom";
            this.ColonnePrenom.ReadOnly = true;
            this.ColonnePrenom.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonnePrenom.Width = 68;
            // 
            // ColonneLicense
            // 
            this.ColonneLicense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneLicense.HeaderText = "License";
            this.ColonneLicense.Name = "ColonneLicense";
            this.ColonneLicense.ReadOnly = true;
            this.ColonneLicense.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneLicense.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColonneLicense.Width = 50;
            // 
            // ColonneModification
            // 
            this.ColonneModification.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneModification.HeaderText = "Modification";
            this.ColonneModification.Name = "ColonneModification";
            this.ColonneModification.ReadOnly = true;
            this.ColonneModification.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneModification.Width = 70;
            // 
            // ColonneSuppression
            // 
            this.ColonneSuppression.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColonneSuppression.HeaderText = "Suppression";
            this.ColonneSuppression.Name = "ColonneSuppression";
            this.ColonneSuppression.ReadOnly = true;
            this.ColonneSuppression.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColonneSuppression.Width = 71;
            // 
            // AnnuaireCoureurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 480);
            this.Controls.Add(this.BouttonAjouter);
            this.Controls.Add(this.Grille);
            this.Name = "AnnuaireCoureurs";
            this.Text = "AnnuaireCoureurs";
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Grille;
        private System.Windows.Forms.Button BouttonAjouter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneNom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonnePrenom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColonneLicense;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneModification;
        private System.Windows.Forms.DataGridViewButtonColumn ColonneSuppression;
    }
}