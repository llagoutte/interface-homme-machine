﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCourseOrientation
{
    public partial class Accueil : Form
    {
        private GestionCO GestionCO;
        public Accueil()
        {
            InitializeComponent();
            GestionCO = new GestionCO();
        }

        private void BoutonNouvelleCourse_Click(object sender, EventArgs e)
        {
            GestionCO.NouvelleCourse();
        }

        private void BouttonQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BoutonOuvrirCourse_Click(object sender, EventArgs e)
        {
            GestionCO.OuvrirCourse();
        }

        private void BoutonAnnuaire_Click(object sender, EventArgs e)
        {
            GestionCO.AfficherAnnuaireCoureur();
        }
    }
}
