﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ajout de la référence 'OneWireLinkLayer' au projet
using DalSemi.OneWire.Adapter;
using DalSemi.OneWire;
// ajout des références pour la gestion de la base de données
using System.Data.SqlClient;

namespace TagCoureur
{
    class CIButton
    {
        // adresse de l'appareil
        private byte[] internAddress = new byte[8];

        public bool searchFamilyDevice(ref PortAdapter adapter, int family)
        {
            try
            {
                if (adapter != null)
                {
                    initCommand(ref adapter);
                    // configure le réseau 1-Wire pour rechercher tous les iButtons et périphériques
                    adapter.SetSearchAllDevices();
                    // applique la vitesse du 1-Wire à 'SPEED_REGULAR'
                    adapter.Speed = OWSpeed.SPEED_REGULAR;
                    // sélectionne le iButton lors de la recherche en prenant en compte la famille
                    adapter.TargetFamily(family);

                    // obtention du premier appareil de la famille
                    if (adapter.GetFirstDevice(internAddress, 0)) return true;
                    else return false;
                }
                else return false;
            }
            catch (AdapterException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void initCommand(ref PortAdapter adapter)
        {
            // réinitialise le réseau 1-Wire
            adapter.Reset();
            // sélectionne le iButton sans recherche (fonctionne seulement avec un seul objet sur le détecteur)
            adapter.PutByte(0xCC);
        }

        public static string getHexAddress(byte[] buffer)
        {
            // modification de la chaîne sans création d'objet
            StringBuilder sb = new StringBuilder(buffer.Length * 3);
            // rangement prêt à l'emploi
            //for (int i = 0; i < 8; i++) sb.Append(buffer[i].ToString("X2"));
            // rangement affichage (par défaut)
            for (int i = 7; i >= 0; i--) sb.Append(buffer[i].ToString("X2"));
            return sb.ToString();
        }

        public string getInternAddress()
        {
            return getHexAddress(internAddress);
        }
    }

    class CTag : CIButton
    {
        public bool initTag(ref PortAdapter adapter, int family, int idCoureur)
        {
            try
            {
                byte id = Convert.ToByte(idCoureur);
                bool retourSearchFamily = false, retourReset = false;
                // recherche d'un iButton avec le code famille demandé
                retourSearchFamily = searchFamilyDevice(ref adapter, family);
                if (!retourSearchFamily) return false;
                // réinitialisation de la première page de la mémoire
                retourReset = resetTag(ref adapter, id);
                if (!retourReset) return false;
                else return true;
            }
            catch (AdapterException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool readTag(ref PortAdapter adapter, int address, ref string tagMem, ref int cpt)
        {
            try
            {
                byte[] data = new byte[8];

                initCommand(ref adapter);
                // émission d'une commande de lecture de la mémoire du iButton
                adapter.PutByte(0xF0);
                // adresse cible TA1
                adapter.PutByte(address & 0x0);
                // adresse cible TA2
                adapter.PutByte((address >> 8) & 0x0);
                // récupération de la valeur du compteur de balises
                cpt = adapter.GetByte();
                if (cpt == 0) return false;
                // calcul de la page et de l'offset actuel
                int page = 0, offset = 0;
                for (int tmp = 0; tmp <= cpt; tmp++)
                {
                    if (tmp % 4 == 0)
                    {
                        page++;
                        offset = 0;
                    }
                    else offset += 8;
                }

                // relecture de la page de gestion
                for (int i = 0; i < 32; i++) adapter.GetByte();
                // boucle permettant la lecture de chaque balise
                for (address = 0x20; address <= ((page * 0x20) + offset - 0x8); address += 0x8)
                {
                    // initialisation des paramètres
                    int nBalise = 0;
                    string tpsBals = "";

                    // récupération des données d'une balise
                    adapter.GetBlock(data, 8);
                    // décomposition des données
                    for (int i = 0; i < 7; i++)
                    {
                        if (i < 4) nBalise += (int)data[i];
                        else tpsBals += data[i].ToString("X2");
                    }
                    // convertion du temps (en hexa) en entier lisible par l'utilisateur
                    int tpsBal = Convert.ToInt32(tpsBals, 16);
                    tagMem += "Balise " + nBalise + ": " + tpsBal + " \n";
                }
                calculateTime(ref tagMem, cpt);
                return true;
            }
            catch (AdapterException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool resetTag(ref PortAdapter adapter, int id)
        {
            try
            {
                int address = 0x0;
                // initialise le scratchpad du iButton
                writeScratchpad(ref adapter, address, id);
                // copie le scratchpad dans la première page de la mémoire
                copyScratchpad(ref adapter, address);
                return true;
            }
            catch (AdapterException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

       /* public bool checkTag(ref PortAdapter adapter, int address, ref string tagMem)
        {
            bool retourRead = false;
            int cpt = 0;

            // ==================== !!! ====================
            CTicket db = new CTicket();
            string parcoursDB = "";

            db.initDB();
            bool retourConnectDB = db.connectToDB();
            if (!retourConnectDB) return false;

            // création d'une commande mysql et envoi de la requête
            SqlCommand command = db.connexion.CreateCommand();
            command.CommandText = "SELECT numBalise FROM Balise;";

            // lecture de chaque ligne de la table
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read()) parcoursDB += reader.ToString() + ';';
            Console.WriteLine(parcoursDB);

            retourConnectDB = db.deconnectToDB();
            if (!retourConnectDB) return false;
            // ==================== !!! ====================

            retourRead = readTag(ref adapter, address, ref tagMem, ref cpt);

            string parcoursTag = "";
            string[] splitSpace = tagMem.Split(' ');

            int j = 1;
            for (int i = 0; i < cpt; i++)
            {
                parcoursTag += splitSpace[j] + ';';
                j += 2;
            }

            if (retourRead && tagMem != "" && parcoursTag == parcoursDB)
            {
                //Console.WriteLine(tagMem);
                //MessageBox.Show(tagMem);
                return true;
            }
            else return false;
        }
        */
        private void writeScratchpad(ref PortAdapter adapter, int address, int id)
        {
            initCommand(ref adapter);
            // émission d'une commande d'écriture dans le scratchpad
            adapter.PutByte(0x0F);
            // adresse cible TA1
            adapter.PutByte(address & 0x0);
            // adresse cible TA2
            adapter.PutByte((address >> 8) & 0x0);
            // initialisation du scratchpad
            for (int i = 0; i < 32; i++)
            {
                // écriture de l'ID du coureur
                if (i == 5) adapter.PutByte(id);
                else adapter.PutByte(0x0);
            }
        }

        private void copyScratchpad(ref PortAdapter adapter, int address)
        {
            initCommand(ref adapter);
            // émission d'une commande de copie du scratchpad dans la mémoire
            adapter.PutByte(0x55);
            // adresse cible TA1
            adapter.PutByte(address & 0x0);
            // adresse cible TA2
            adapter.PutByte((address >> 8) & 0x0);
            // fin d'adresse avec état de données E/S
            adapter.PutByte(0x1F);
            // temps d'écriture dans la mémoire
            System.Threading.Thread.Sleep(30);
        }

        public void calculateTime(ref string tagMem, int cpt)
        {
            // décompose le contenu du iButton par ligne
            string[] splitLine = tagMem.Split('\n');
            // création d'un tableau de cpt cases de tableaux de 3 chaînes de caractères
            string[,] splitMem = new string[cpt, 3];
            // création d'un contenu mémoire tampon
            string newTagMem = "";

            for (int i = 0; i < cpt; i++)
            {
                // décompose chaque ligne par un tableau dont chaque case est séparé par un ' '
                string[] splitSpace = splitLine[i].Split(' ');
                // remplissage du tableau
                for (int j = 0; j < 3; j++) splitMem[i, j] = splitSpace[j];
                // calcule de l'écart de temps par rapport à la première balise
                int tpsBal = Convert.ToInt32(splitMem[i, 2]) - Convert.ToInt32(splitMem[0, 2]);
                // changement du format du temps: sec => hh:mm:ss
                string tpsBals = s_to_hms(tpsBal);
                // remplissage du contenu mémoire tampon
                newTagMem += splitMem[i, 0] + " " + splitMem[i, 1] + " " + tpsBals + "\n";
            }
            // écriture du nouveau contenu mémoire
            tagMem = newTagMem;
        }

        private string s_to_hms(int s)
        {
            // retourne une erreur si le paramètre est négatif ou trop grand (supérieur à 86400)
            if (s < 0) return "Impossible de faire la conversion: nombre de secondes négatif !";
            else if (s > 86400) return "Impossible de faire la conversion: nombre de secondes trop grand (supérieur à 24h) !";
            // sinon calcule le nouveau format
            else
            {
                int h = 0, m = 0;
                while (s >= 60 || m >= 60)
                {
                    if (s >= 60)
                    {
                        m++;
                        s -= 60;
                    }
                    else if (m >= 60)
                    {
                        h++;
                        m -= 60;
                    }
                }
                string str = h + ":" + m + ":" + s;
                return str;
            }
        }
    }
}