﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionCourseOrientation.Courses.Configurations.ConfigurationsBalises;
using GestionCourseOrientation.Courses.Configurations.Participants;

namespace GestionCourseOrientation
{
    public class Course
    {
        private string _NomCourse;
        private string _NomOrganisateur;
        private string _TypeCourse;
        private string _Date;
        private string _Lieu;
        private List<Balise> Balises;
        private List<Participant> Participants;

        public Course()
        {
            Balises = new List<Balise>();
            Participants = new List<Participant>();
        }

        public string NomCourse
        {
            set { _NomCourse = value; }
            get { return _NomCourse; }
        }

        public string NomOrganisateur
        {
            set { _NomOrganisateur = value; }
            get { return _NomOrganisateur; }
        }

        public string TypeCourse
        {
            set { _TypeCourse = value; }
            get { return _TypeCourse; }
        }

        public string Date
        {
            set { _Date = value; }
            get { return _Date; }
        }

        public string Lieu
        {
            set { _Lieu = value; }
            get { return _Lieu; }
        }

        public void ConfigurerBalises()
        {
            ConfigurationBalises ConfigurationDesBalises = new ConfigurationBalises(ref Balises);
            ConfigurationDesBalises.ShowDialog();
        }

        public void ConfigurerParticipants()
        {
            ConfigurationParticipants ConfigurationDesParticipants = new ConfigurationParticipants(ref Participants);
            ConfigurationDesParticipants.ShowDialog();

        }
    }

    public class Coureur
    {
        private string _NomCoureur;
        private string _PrenomCoureur;
        private string _License;

        public string NomCoureur
        {
            set { _NomCoureur = value; }
            get { return _NomCoureur; }
        }

        public string PrenomCoureur
        {
            set { _PrenomCoureur = value; }
            get { return _PrenomCoureur; }
        }

        public string License
        {
            set { _License = value; }
            get { return _License; }
        }
    }

    public class Participant : Coureur
    {
        private int _TagC;
        private string _Dossard;
        private static int Id=0;

        public Participant()
        {
        }

        public static int GetNewId()
        {
            Id++;
            return Id;
        }

        public string Dossard
        {
            get { return _Dossard; }
            set { _Dossard = value; }
        }

        public int TagC
        {
            get { return _TagC; }
            set { _TagC = value; }
        }
    }

    public class Balise
    {
        private byte _Identifiant;
        private byte _Numero;
        private float _GPS;

        public byte Identifiant
        {
            get { return _Identifiant; }
            set { _Identifiant = value; }
        }

        public byte Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        public float GPS
        {
            get { return _GPS; }
            set { _GPS = value; }
        }

        public bool Sauvegarder(){
            bool Succes = true;

            //Succes = BaseDonneesCO :: AjouterBalise(this);  //On ajoute la balise dans la base de données

            if(Succes == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class GestionCO
    {
        private List<Course> Courses;
        private static List<Coureur> Coureurs;

        public GestionCO()
        {
            Courses = new List<Course>();
            Coureurs = new List<Coureur>();
        }

        public static List<Coureur> Annuaire()
        {
            return Coureurs;
        }

        public void NouvelleCourse()
        {
            Course course = new Course();
            NouvelleCourse NouvelleCourse = new NouvelleCourse(ref course);
            NouvelleCourse.ShowDialog();
            if(NouvelleCourse._ChampsValide==true)
            {
                Courses.Add(course);
            }
        }

        public void OuvrirCourse()
        {
            Course course = new Course();
            int Index = -1;

            SelectionCourse CourseSelectionne = new SelectionCourse(Courses);
            CourseSelectionne.ShowDialog();

            Index = CourseSelectionne.Index;

            if(Index!=-1) // Vérification si la course est bel et bien disponible
            {
                course = Courses[Index];
                ConfigurationCourse ConfigCourse = new ConfigurationCourse(ref course);
                ConfigCourse.ShowDialog();
                Courses[Index] = course;
            }

        }

        public void AfficherAnnuaireCoureur()
        {
            AnnuaireCoureurs FenetreAnnuaire = new AnnuaireCoureurs(ref Coureurs);
            FenetreAnnuaire.ShowDialog();

        }

    }

}
