﻿namespace GestionCourseOrientation
{
    partial class Accueil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Menu = new System.Windows.Forms.ToolStripMenuItem();
            this.LabelAccueil = new System.Windows.Forms.Label();
            this.BoutonNouvelleCourse = new System.Windows.Forms.Button();
            this.BoutonOuvrirCourse = new System.Windows.Forms.Button();
            this.BoutonAnnuaire = new System.Windows.Forms.Button();
            this.BouttonQuitter = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(516, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Menu
            // 
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(50, 20);
            this.Menu.Text = "Menu";
            // 
            // LabelAccueil
            // 
            this.LabelAccueil.AutoSize = true;
            this.LabelAccueil.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAccueil.Location = new System.Drawing.Point(205, 35);
            this.LabelAccueil.Name = "LabelAccueil";
            this.LabelAccueil.Size = new System.Drawing.Size(88, 23);
            this.LabelAccueil.TabIndex = 2;
            this.LabelAccueil.Text = "Accueil";
            // 
            // BoutonNouvelleCourse
            // 
            this.BoutonNouvelleCourse.Location = new System.Drawing.Point(194, 131);
            this.BoutonNouvelleCourse.Name = "BoutonNouvelleCourse";
            this.BoutonNouvelleCourse.Size = new System.Drawing.Size(111, 53);
            this.BoutonNouvelleCourse.TabIndex = 3;
            this.BoutonNouvelleCourse.Text = "Nouvelle\r\nCourse";
            this.BoutonNouvelleCourse.UseVisualStyleBackColor = true;
            this.BoutonNouvelleCourse.Click += new System.EventHandler(this.BoutonNouvelleCourse_Click);
            // 
            // BoutonOuvrirCourse
            // 
            this.BoutonOuvrirCourse.Location = new System.Drawing.Point(194, 239);
            this.BoutonOuvrirCourse.Name = "BoutonOuvrirCourse";
            this.BoutonOuvrirCourse.Size = new System.Drawing.Size(111, 53);
            this.BoutonOuvrirCourse.TabIndex = 4;
            this.BoutonOuvrirCourse.Text = "Ouvrir\r\nCourse";
            this.BoutonOuvrirCourse.UseVisualStyleBackColor = true;
            this.BoutonOuvrirCourse.Click += new System.EventHandler(this.BoutonOuvrirCourse_Click);
            // 
            // BoutonAnnuaire
            // 
            this.BoutonAnnuaire.Location = new System.Drawing.Point(194, 353);
            this.BoutonAnnuaire.Name = "BoutonAnnuaire";
            this.BoutonAnnuaire.Size = new System.Drawing.Size(111, 53);
            this.BoutonAnnuaire.TabIndex = 5;
            this.BoutonAnnuaire.Text = "Annuaire\r\nCoureur";
            this.BoutonAnnuaire.UseVisualStyleBackColor = true;
            this.BoutonAnnuaire.Click += new System.EventHandler(this.BoutonAnnuaire_Click);
            // 
            // BouttonQuitter
            // 
            this.BouttonQuitter.Location = new System.Drawing.Point(194, 515);
            this.BouttonQuitter.Name = "BouttonQuitter";
            this.BouttonQuitter.Size = new System.Drawing.Size(111, 53);
            this.BouttonQuitter.TabIndex = 6;
            this.BouttonQuitter.Text = "Quitter";
            this.BouttonQuitter.UseVisualStyleBackColor = true;
            this.BouttonQuitter.Click += new System.EventHandler(this.BouttonQuitter_Click);
            // 
            // Accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 580);
            this.Controls.Add(this.BouttonQuitter);
            this.Controls.Add(this.BoutonAnnuaire);
            this.Controls.Add(this.BoutonOuvrirCourse);
            this.Controls.Add(this.BoutonNouvelleCourse);
            this.Controls.Add(this.LabelAccueil);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Accueil";
            this.Text = "Gestion de course d\'orientation";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Menu;
        private System.Windows.Forms.Label LabelAccueil;
        private System.Windows.Forms.Button BoutonNouvelleCourse;
        private System.Windows.Forms.Button BoutonOuvrirCourse;
        private System.Windows.Forms.Button BoutonAnnuaire;
        private System.Windows.Forms.Button BouttonQuitter;
    }
}

